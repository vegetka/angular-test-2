import { DirectivesEx400Page } from './app.po';

describe('directives-ex400 App', function() {
  let page: DirectivesEx400Page;

  beforeEach(() => {
    page = new DirectivesEx400Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
