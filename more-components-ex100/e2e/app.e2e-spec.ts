import { MoreComponentsEx100Page } from './app.po';

describe('more-components-ex100 App', function() {
  let page: MoreComponentsEx100Page;

  beforeEach(() => {
    page = new MoreComponentsEx100Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
