import { DirectivesEx700Page } from './app.po';

describe('directives-ex700 App', function() {
  let page: DirectivesEx700Page;

  beforeEach(() => {
    page = new DirectivesEx700Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
