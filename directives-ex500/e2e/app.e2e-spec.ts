import { DirectivesEx500Page } from './app.po';

describe('directives-ex500 App', function() {
  let page: DirectivesEx500Page;

  beforeEach(() => {
    page = new DirectivesEx500Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
