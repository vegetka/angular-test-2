import { DirectivesEx600Page } from './app.po';

describe('directives-ex600 App', function() {
  let page: DirectivesEx600Page;

  beforeEach(() => {
    page = new DirectivesEx600Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
